package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type User struct {
	Name string `json:"name"`
	Age  string `json:"age"`
	Job  string `json:"job"`
}

var users []User

func init() {
	users = []User{
		{"Snoop Dogg", "47 tuổi", "Ca sĩ, Diễn viên"},
		{"Drake", "32 tuổi", "Ca sĩ"},
		{"Selena Gomez", "27 tuổi", "Ca sĩ"},
		{"David Beckham", "44 tuổi", "Cầu thủ bóng đá"},
		{"Ronaldinho", "39 tuổi", "Cầu thủ bóng đá"},
	}

	for i := 0; i < 1000; i++ {
		u := User{fmt.Sprintf("User %d", i), fmt.Sprintf("%d tuổi", i), "Unknown"}
		users = append(users, u)
	}
}

func main() {
	engine := gin.Default()

	engine.GET("/", func(c *gin.Context) {
		name := c.DefaultQuery("name", "World!")
		c.String(http.StatusOK, fmt.Sprintf("Hello %s", name))
	})

	engine.GET("/users", func(c *gin.Context) {

		c.JSON(http.StatusOK, users)
	})

	engine.Run(":8000")
}
